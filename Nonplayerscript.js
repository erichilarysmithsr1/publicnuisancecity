let manPosition = 0;
let alreadyShot = false;
let arrowsLeft = 10;
let zombiesKilled = 0;
let dead = false;
let addEnemyRate = .2;
let addPackageRate = .05;

setInterval(updateGameTypes, 300);
setInterval(makeHarder, 10000);

let gameTypes = [];

function gameType(position, emoji){
  this.position = position;
  this.emoji = emoji;
  gameTypes.push(this);
}

for(let x = 0; x < 5; x++){
  new gameType(x + 50,  "🧟");
}

function updateGameTypes(){
  let addEnemy = Math.random();
  if(addEnemy < addEnemyRate){
     new gameType(50,  "🧟");
  }
  let addPackage = Math.random();
  if(addPackage < addPackageRate && !dead){
    new gameType(Math.floor(Math.random() * 25),  "📦");
  }
  alreadyShot = false;
  for(let x = 0; x < gameTypes.length; x++){
    if(gameTypes[x].emoji === "🧟"){
      gameTypes[x].position--;
      if(gameTypes[x].position < 0){
        gameTypes.splice(x, 1);
        x--
      }
    }else if(gameTypes[x].emoji === "➡️"){
      gameTypes[x].position++;
      if(gameTypes[x].position > 100){
        gameTypes.splice(x, 1);
        x--
      }
    }else if(gameTypes[x].emoji === "💥"){
      gameTypes.splice(x, 1);
      x--;
    }
  }
  checkDeath();
  checkCollision();
  updateGame();
}

function checkCollision(){
   for(let x = 0; x < gameTypes.length; x++){
     for(let y = 0; y < gameTypes.length; y++){
        if(gameTypes[x] && (gameTypes[x].position == gameTypes[y].position || gameTypes[x].position == gameTypes[y].position-1)){
           if(gameTypes[x].emoji == "🧟" && gameTypes[y].emoji == "➡️"){
             new gameType(gameTypes[x].position, "💥");
             gameTypes[x].position = -1;
             gameTypes[y].position = 101;
             zombiesKilled++;
           }
        }
        if(gameTypes[x].emoji === "📦" && (gameTypes[x].position == manPosition || gameTypes[x].position == manPosition + 1)){
          arrowsLeft += 5;
          gameTypes.splice(x, 1);
          x--;
        }
     }
   }
}

function checkDeath(){
  for(let x = 0; x < gameTypes.length; x++){
    if(gameTypes[x].position == manPosition && gameTypes[x].emoji == "🧟"){
      dead = true;
      document.getElementById("restart").style = "display: block";
    }
  }
}
  
function updateGame(){
  checkCollision();
  let title = "";
  for(let x = 0; x < 51; x++){
     if(manPosition == x){
       if(!dead){
        title +=  "🧔🏹";
        x++; 
       }else{
        title +=  "☠️";
       }
     }else{
       let found = false;
       for(let y = 0; y < gameTypes.length; y++){
          if(gameTypes[y].position == x){
            title += gameTypes[y].emoji;
            found = true;
          }
       }
       if(!found){
          title += "󠀠⠀"; 
       }
     }
  }
  title = "⠀Arrows:⠀" + arrowsLeft + "⠀" +  title;
  document.getElementById("zombies-killed").innerText = "Zombies Killed: " + zombiesKilled;
  document.title = title;
  window.history.pushState({}, title, "#"+ title);
}

window.addEventListener("keydown", e => {
  if(dead){
    return; 
  }
  if(e.keyCode == 39){
    if(manPosition < 50){
      manPosition++;
    }
  }
  if(e.keyCode == 37){
    if(manPosition > 0){
      manPosition--;
    }
  }
  if(e.keyCode == 32 && !alreadyShot && arrowsLeft > 0){
     new gameType(manPosition + 2, "➡️");
     alreadyShot = true;
     arrowsLeft--;
  }
  checkDeath();
  updateGame();
});

function restart(){
  gameTypes = [];
  manPosition = 0;
  alreadyShot = false;
  arrowsLeft = 10;
  zombiesKilled = 0;
  dead = false;
  addEnemyRate = .2;
  addPackageRate = .05;
  
  
  for(let x = 0; x < 5; x++){
    new gameType(x + 50,  "🧟");
  }
  document.getElementById("restart").style = "display: none";
}

function makeHarder(){
    addEnemyRate += .03;
    addPackageRate -= .001;
}

updateGameTypes();
